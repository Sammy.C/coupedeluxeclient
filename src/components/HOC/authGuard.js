import React, { Component } from "react"
import {connect} from 'react-redux'

export default (OriginalComponent) =>{
    class MixedComponent extends Component {
        checkAuth(){
            if(!this.props.isAunthenticated && !this.props.jwt){
                this.props.history.push('/login')
            }
        }
        componentDidMount(){
            //check if the user is authenticated
            this.checkAuth()
        }
        componentDidUpdate(){
            this.checkAuth()
        }
        render(){
            return(
                <OriginalComponent {...this.props}/>
            )
        }
    }
    function mapStateToProps(state){
        return{
            isAunthenticated: state.authentication.isAuthenticated,
            jwt: state.authentication.key
        }
    }
    return connect(mapStateToProps)(MixedComponent)
}