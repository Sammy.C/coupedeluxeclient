import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="ftco-footer ftco-section img">
        <div className="overlay" />
        <div className="container">
          <div className="row mb-5">
            <div className="col-md-3">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">About Us</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <ul className="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                  <li className="ftco-animate"><div><span className="icon-twitter" /></div></li>
                  <li className="ftco-animate"><div><span className="icon-facebook" /></div></li>
                  <li className="ftco-animate"><div><span className="icon-instagram" /></div></li>
                </ul>
              </div>
            </div>
            <div className="col-md-4">
            </div>
            <div className="col-md-2">
              <div className="ftco-footer-widget mb-4 ml-md-4">
                <h2 className="ftco-heading-2">Services</h2>
                <ul className="list-unstyled">
                  <li><p className="py-2 d-block">Haircut</p></li>
                  <li><p className="py-2 d-block">Hairstyle</p></li>
                  <li><p className="py-2 d-block">Trimming</p></li>
                  <li><p className="py-2 d-block">Shaving</p></li>
                  <li><p className="py-2 d-block">Skin Care</p></li>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Have a Questions?</h2>
                <div className="block-23 mb-3">
                  <ul>
                    <li><span className="icon icon-map-marker" /><span className="text">264 St-Paul Street est. Suite 100 Montreal QC</span></li>
                    <li><div><span className="icon icon-phone" /><span className="text">514) 570-0953</span></div></li>
                    <li><div><span className="icon icon-envelope" /><span className="text">TBD</span></div></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center">
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
export default Footer;
