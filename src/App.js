import React, {useState } from "react";
import { I18nPropvider, LOCALES } from './i8n';
import translate from "./i8n/translate";
function App() {
  const [locale, setLocale] = useState(LOCALES.ENGLISH);
  return (
      <I18nPropvider locale={locale}>
      <div>
        <nav
        className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
        id="ftco-navbar"
      >
        <div className="container">
          <a className="navbar-brand" href="/">
            CoupeDeLuxe
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#ftco-nav"
            aria-controls="ftco-nav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="oi oi-menu"></span> Menu
          </button>

          <div className="collapse navbar-collapse" id="ftco-nav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a href="/" className="nav-link">
                {translate('home')}
                </a>
              </li>
              <li className="nav-item">
                <a href="#about" className="nav-link">
                {translate('about')}
                </a>
              </li>
              <li className="nav-item">
                <a href="#services" className="nav-link">
                  {translate('services')}
                </a>
              </li>
              <li className="nav-item">
                <a href="#contact" className="nav-link">
                {translate('contact')}
                </a>
              </li>
              <li className="nav-item">
                <a href="#section-counter" className="nav-link">
                {translate('careers')}
                </a>
              </li>
              <li className="nav-item">
                <a href="https://www.fresha.com/providers/coupe-de-luxe-iox8gjg2?pId=363496" className="nav-link">
                {translate('book_now')}
                </a>
              </li>
              <li className="nav-item">
                <p href="/" onClick={() => locale === LOCALES.ENGLISH ? setLocale(LOCALES.FRENCH) : setLocale(LOCALES.ENGLISH)} className="nav-link">
                  {locale === LOCALES.ENGLISH ? "Fr": "En"}
                </p>
              </li>
            </ul>
          </div>
        </div>
      </nav>
        <div
          className="hero-wrap js-fullheight"
          style={{ backgroundImage: "url(images/bg_2.png)" }}
          data-stellar-background-ratio="0.5"
        >
          <div className="overlay" />
          <div className="container">
            <div className="row no-gutters slider-text js-fullheight align-items-center justify-content-start">
             
              <div className="col-md-6 ">
                <p className="mb-3" />
                <h1
                  className="mb-5"
                  style={{
                    opacity: "0.847909",
                    transform: "translateZ(0px) translateY(2.85171%)"
                  }}
                >
                  {translate('intro')}
                </h1>
                <p>
                  <a href="https://www.fresha.com/providers/coupe-de-luxe-iox8gjg2?pId=363496" className="btn btn-primary px-4 py-3">
                  {translate('book_appointment')}
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <section className="ftco-intro">
          <div className="container-wrap">
            <div className="wrap d-md-flex align-items-end">
              <div className="info">
                <div className="row no-gutters">
                  <div className="col-md-4 d-flex ">
                    <div className="icon">
                      <span className="icon-phone" />
                    </div>
                    <div className="text">
                      <h3><a href="tel:+1438-380-3700">438-380-3700</a></h3>
                    </div>
                  </div>
                  <div className="col-md-4 d-flex ">
                    <div className="icon">
                      <span className="icon-my_location" />
                    </div>
                    <div className="text">
                      <h3>264 St-Paul Street est.</h3>
                      <p>Suite 100 Montreal QC</p>
                    </div>
                  </div>
                  <div className="col-md-4 d-flex ">
                    <div className="icon">
                      <span className="icon-clock-o" />
                    </div>
                    <div className="text">
                      <h3>{translate('opening_hours')}</h3>
                      <p>11:00am - 7:00pm</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="social pl-md-5 p-4">
                <ul className="social-icon">
                  <li className=""><a href="https://www.facebook.com/Coupe-de-Luxe-105523221143783/"><span className="icon-facebook"></span></a></li>
                 <li className=""><a href="https://www.instagram.com/coupe.deluxe/"><span className="icon-instagram"></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section className="ftco-section" id="about">
    	<div className="container">
    		<div className="row justify-content-center mb-4">
          <div className="col-md-7 heading-section  text-center" >
			<h1 className="">{translate('welcome_to')}<span className="text-primary">Coupe De Luxe</span></h1>
			<h3 className="mb-4">{translate('gentlemen')}</h3>
            <p className="flip"><span className="deg1"></span><span className="deg2"></span><span className="deg3"></span></p>
          </div>
        </div>
    		<div className="row justify-content-center">
    			<div className="col-md-8 text-center ">
    				<p>{translate('barber_into')}</p>
    			</div>
    		</div>
    	</div>
    </section>
    <section className="ftco-section ftco-bg-dark"  id="services">
    	<div className="container">
    		<div className="row justify-content-center mb-5 pb-3 mt-5 pt-5">
          <div className="col-md-7 heading-section text-center ">
            <h2 className="mb-4">{translate('plans_pricing')}</h2>
            <p className="flip"><span className="deg1"></span><span className="deg2"></span><span className="deg3"></span></p>
          </div>
        </div>
        <div className="row">
        	<div className="col-md-6">
        		<div className="pricing-entry ">
        			<div className="d-flex text align-items-center">
        				<h3><span>{translate('haircut')}</span></h3>
        				<span className="price">$37.00</span>
        			</div>
        			<div className="d-block">

        			</div>
        		</div>
        		<div className="pricing-entry ">
        			<div className="d-flex text align-items-center">
        				<h3><span>{translate('beard')}</span></h3>
        				<span className="price">$20.00</span>
        			</div>
        			<div className="d-block">
        			
        			</div>
        		</div>
        		<div className="pricing-entry ">
        			<div className="d-flex text align-items-center">
        				<h3><span>{translate('bronzage')}</span></h3>
        				<span className="price">$22.00/10Min</span>
        			</div>
        			<div className="d-block">
        				
        			</div>
        		</div>
        	</div>

        	<div className="col-md-6">
        		<div className="pricing-entry ">
        			<div className="d-flex text align-items-center">
        				<h3><span>{translate('haircut_beard')}</span></h3>
        				<span className="price">$50.00</span>
        			</div>
        			<div className="d-block">
        			
        			</div>
        		</div>
        		<div className="pricing-entry ">
        			<div className="d-flex text align-items-center">
        				<h3><span>{translate("hair_wash")}</span></h3>
        				<span className="price">$11.00</span>
        			</div>
        			<div className="d-block">
        			
        			</div>
        		</div>
        	</div>
        </div>
    	</div>
      </section>
      <section className="ftco-section ftco-discount img" style={{backgroundImage: 'url(images/bg_4.jpg)'}} data-stellar-background-ratio="0.5">
			<div className="overlay" style={{opacity: 0.8}}></div>
			<div className="container">
				<div className="row justify-content-center" data-scrollax-parent="true">
					<div className="col-md-7 text-center discount " data-scrollax=" properties: { translateY: '-30%'}">
						
						<h2 className="mb-4">{translate('covid_19')}</h2>
						<p className="mb-4 text-white">{translate('covid_19_description')}</p>
					</div>
				</div>
			</div>
		</section>
    <section className="ftco-section ftco-bg-dark">
    <div className="row justify-content-center mb-5 pb-3 mt-5 pt-5">
          <div className="col-md-7 heading-section text-center ">
            <h2 className="mb-4">Gallery</h2>
            <p className="flip"><span className="deg1"></span><span className="deg2"></span><span className="deg3"></span></p>
          </div>
        </div>
    <div class="container-wrap">
	    		<div class="row ">
						  <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-1.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-2.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-3.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-4.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-5.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                    <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-6.jpg)"}}></div>
              </div>
              <div class="col-md-3">
						
                       <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-7.jpg)"}}></div>
            </div>
            <div class="col-md-3">
						
                       <div className="gallery img  d-flex align-items-center mb-4" style={{backgroundImage: "url(images/work-8.jpg)"}}></div>
            </div>
						</div>
          </div>
	  </section>
    <section className="ftco-counter ftco-bg-dark img" id="section-counter" style={{backgroundImage: 'url(images/bg_1.jpg)'}} data-stellar-background-ratio="0.5">
			<div className="overlay"></div>
      <div className="container justify-content-center align-items-center">
        <div className="row justify-content-center align-items-center">
	    		<div className="col-md-12  justify-content-center align-items-center">
	    			<h3 className="mb-3 text-center">{translate("careers")}</h3>
					<h5 className="text-center">{translate("careers_description")}</h5>
				</div>   
				<a href={  locale === LOCALES.ENGLISH ? "https://docs.google.com/forms/d/e/1FAIpQLSdxTB72JmbUPka77UOS-fduoxg1idx69ANCoQgLA6zl3yvcFw/viewform?usp=pp_url" : "https://docs.google.com/forms/d/e/1FAIpQLSdvzxwgRD509o9wWgQ00em-ySyu-5Ttebgs5H42FyOXXLUwEg/viewform?usp=pp_url"} className="col-md-4   justify-content-center align-items-center btn btn-primary py-3 px-4">{translate("apply")}</a> 	
				
        </div>
      </div>
    </section>
    
	<section className="ftco-appointment">

  </section>
  <footer className="ftco-footer ftco-section img">
    	<div className="overlay"></div>
      <div className="container">
        <div className="row mb-5">
          <div className="col-md-4">
            <div className="ftco-footer-widget mb-4">
              <h2 className="ftco-heading-2">{translate('social_media')}</h2>
              <ul className="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li className=""><a href="https://www.facebook.com/Coupe-de-Luxe-105523221143783/"><span className="icon-facebook"></span></a></li>
                <li className=""><a href="https://www.instagram.com/coupe.deluxe/"><span className="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div className="col-md-4" >
             <div className="ftco-footer-widget mb-4 ml-md-4">
              <h2 className="ftco-heading-2">{translate("services")}</h2>
              <ul className="list-unstyled">
                <li><a href="#services" className="py-2 d-block">{translate('haircut')}</a></li>
                <li><a href="#services" className="py-2 d-block">{translate('beard')}</a></li>
                <li><a href="#services" className="py-2 d-block">{translate('bronzage')}</a></li>
                <li><a href="#services" className="py-2 d-block">{translate('haircut_beard')}</a></li>
                <li><a href="#services" className="py-2 d-block">{translate('hair_wash')}</a></li>
              </ul>
            </div>
          </div>
          <div className="col-md-4" id="contact">
            <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">{translate("questions")}</h2>
            	<div className="block-23 mb-3">
	              <ul>
	                <li><span className="icon icon-map-marker"></span><span className="text">264 St-Paul Street est. Suite 100,Montreal, Quebec, CA</span></li>
	                <li><a  href="tel:+1438-380-3700"><span className="icon icon-phone"></span><span className="text">438-380-3700</span></a></li>
	                <li><a href="mailto:contact@coupedeluxe.com"><span className="icon icon-envelope"></span><span className="text">contact@coupedeluxe.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 text-center">

            <p></p>
          </div>
        </div>
      </div>
    </footer>
      </div>
      </I18nPropvider>
  );
}

export default App;
